  
const assert = require('chai').assert;
const exampleModule = require('../service/ejemploModulo');

describe('test de ejemploModulo', () => {
	it('test ejemplo modulo', () => {
		var result = exampleModule.test('Amcl');
		assert.equal('Hola Amcl', result);
	});
});